CC := clang

main: main.o func1.o func2.o
	$(CC) -o $@ $^

.c.o: 
	$(CC) -c -o $@ $<

clean:
	rm -f main *.o
